# -*- coding: utf-8 -*-

import django.dispatch

# Sent after the user's first login:
first_login = django.dispatch.Signal('sender', 'instance')
