# -*- coding: utf-8 -*-

import smtplib
from PIL import Image

from django.core.mail import send_mail

from jobseeker import settings


def newpath(storage, original_path):
    path, filename = original_path.rpartition('/')[0::2]
    filename = filename.rpartition('.')[0] + '.jpg'

    return storage.get_available_name(path + '/' + filename)


def get_thumbnail(instance):
    path = settings.MEDIA_ROOT + 'img/slides/thumbnails/'
    filename = instance.image.name.rpartition('/')[2].lower()
    img = Image.open(instance.image)
    size = (settings.SLIDE_THUMBNAIL_SIZE, settings.SLIDE_THUMBNAIL_SIZE)
    
    img.thumbnail(size, Image.ANTIALIAS)
    fullpath = newpath(instance.thumbnail.storage, path + filename)
    img.save(fullpath, 'JPEG')

    return fullpath.rpartition('uploads/')[2]


def process_img(instance):
    path = settings.MEDIA_ROOT + 'img/slides/'
    filename = instance.image.name.rpartition('/')[2].lower()
    img = Image.open(instance.image)
    if instance.image.width > settings.SLIDE_MAX_WIDTH or instance.image.height > settings.SLIDE_MAX_HEIGHT:
        size = (settings.SLIDE_MAX_WIDTH, settings.SLIDE_MAX_HEIGHT)
        img.thumbnail(size, Image.ANTIALIAS)

    fullpath = newpath(instance.image.storage, path + filename)
    img.save(fullpath, 'JPEG')

    return fullpath.rpartition('uploads/')[2]


def new_company_email(data):
    # Send email to ADMINs when a new company submits the contact form:
        try:
            send_mail(
                data.site.name + ' - Nuevo contacto de empresa',
                'Se ha recibido un nuevo contacto de empresa con los siguientes datos:\n\n'\
                + 'Sitio web: ' + data.site.name + '\n'\
                + 'Empresa: ' + data.name + '\n'\
                + 'Contacto: ' + data.contact + '\n'\
                + 'E-mail: ' + data.contact_email + '\n'\
                + 'Comentarios: ' + '\n' + data.comments + '\n',
                settings.DEFAULT_FROM_EMAIL,
                settings.ADMINS[0],
            )
        except (smtplib.SMTPException):
            pass
