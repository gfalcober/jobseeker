// -----------------------------------------------------------------------------
// Picture slider
// -----------------------------------------------------------------------------
$(document).ready(function() {
    $('#slider').rhinoslider({
        autoPlay: true,
        showTime: 4500,
        effect: 'fade',
        showBullets: 'never',
        controlsMousewheel: false,
        showControls: 'never',
        controlsKeyboard: false,
        showCaptions: 'never'
    });
});


/* -------------------------------------------------------------------------- */
/* Login overlay
/* -------------------------------------------------------------------------- */
$(document).ready(function(){
    $('#fade').popup({
        transition: 'all 0.3s'
    });
});


/* Loading effect on click */
var providers = new Array('google', 'linkedin', 'facebook');

/* Add event listeners */
for(i=0;i<providers.length;i++){
    $('#'+providers[i]+'_login_button').on('click', setSpinner);
}

/* Spinner params */
var opts = {
  lines: 11, // The number of lines to draw
  length: 12, // The length of each line
  width: 6, // The line thickness
  radius: 12, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 39, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: true, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: 'auto', // Top position relative to parent in px
  left: 'auto' // Left position relative to parent in px
};

function setSpinner(){
    var spinner = new Spinner(opts).spin(this);
    /* Remove event listeners */
    for(i=0;i<providers.length;i++){
        $('#'+providers[i]+'_login_button').off();
    }
}


/* Logout buttons */
$('#logout_button').on('click', function(){
    $('#logout_form').submit();
});

$('#logout_button_mobile').on('click', function(){
    $('#logout_form_mobile').submit();
});


/* Mobile menu dropdown button */

$(function(){
    $('#menu').slicknav({
        label: '',
        prependTo: '#dropdown',
        duplicate: false
    });
});

/* Mobile menu close */
$(document).ready(function(){
    $('div.slicknav_menu').mouseleave(function(){
       $('#menu').slicknav('close');
    });
});


/* Smooth scrolling */
$(document).on('click','a.smooth', function(e){
    e.preventDefault();
    var anchor = $(this).attr('href');
    $('html, body').stop().animate({
        scrollTop: $(anchor).offset().top
    }, 900);
});


/* Welcome message */
$(document).ready(function(){
    $('#welcome_message').popup({
        transition: 'all 0.3s'
    });
    $('#welcome_message').popup('show');
});
