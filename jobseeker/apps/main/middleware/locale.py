from django.utils import translation

from jobseeker import settings
from jobseeker.apps.main.models import *

class LocaleSelectorMiddleware(object):
    def process_request(self, request):
        try:
            language = settings.DOMAIN_LANGUAGES[request.META['HTTP_HOST']]['code']
        except (KeyError):
            language = settings.DOMAIN_LANGUAGES['default']['code']


        translation.activate(language)
        request.LANGUAGE_CODE = translation.get_language()
