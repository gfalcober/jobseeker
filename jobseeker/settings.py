# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

ADMINS = (
        ('Ángel Faet', 'unhdlt@gmail.com'),
        ('Gabriel Faet', 'gfalcober@gmail.com'),
)

SECRET_KEY = 'n8dai-6t=)8zr4%l$u@jyn_4#2#cgda)h9kuo*uh0&b=z$40pv'

DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []
SITE_ID = 1

AUTH_USER_MODEL = 'user_engine.User'


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites', #Required by 'allauth'
    'jobseeker.apps.main',
    'jobseeker.apps.user_engine',
    'jobseeker.apps.site_tools.dynamic_site',
    'rosetta',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    #Auth Providers:
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'allauth.socialaccount.providers.linkedin',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.messages.context_processors.messages',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.static',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
)

MIDDLEWARE_CLASSES = (
    'jobseeker.apps.site_tools.dynamic_site.middleware.DynamicSiteMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'jobseeker.apps.main.middleware.locale.LocaleSelectorMiddleware'
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)


ROOT_URLCONF = 'jobseeker.urls'

WSGI_APPLICATION = 'jobseeker.wsgi.application'


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'jobseeker/templates'),
    os.path.join(BASE_DIR, 'jobseeker/templates/account'),
)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'app',
        'USER': 'adminkt9gdaa',
        'PASSWORD': 'JFwTWj5fyKPm',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}


LANGUAGE_CODE = 'es_ES'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'jobseeker/static/')

MEDIA_ROOT = os.path.join(BASE_DIR, 'jobseeker/uploads/')
MEDIA_URL = '/uploaded/'



# --------------------------- EMAIL settings -----------------------------------
if DEBUG is True: EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
else: EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'trotterworldplus'
EMAIL_HOST_PASSWORD = 'T3d1g0tr1g0.'
DEFAULT_FROM_EMAIL = 'TEST@TEST.com'


# ------------------------------------------------------------------------------
# ROSETTA settings
# ------------------------------------------------------------------------------
ROSETTA_MESSAGES_SOURCE_LANGUAGE_CODE = 'es_ES'
ROSETTA_MESSAGES_SOURCE_LANGUAGE_NAME = 'Español'
# This does not work, use --ignore="site_tools*" with makemessages, instead.
ROSETTA_EXCLUDED_APPLICATIONS = ('jobseeker.apps.site_tools.dynamic_site',)


ROSETTA_ENABLE_TRANSLATIONS_SUGGESTIONS = True

LANGUAGES = (
    ('es_GQ', _('Guinea Ecuatorial')),
    ('es_PA', _('Panamá')),
    ('es_PR', _('Puerto Rico')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'jobseeker/locale'),
)


# ------------------------------------------------------------------------------
# ALLAUTH settings
# ------------------------------------------------------------------------------
LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/'

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 1
ACCOUNT_EMAIL_SUBJECT_PREFIX = _('TEST email verification')


# ------------------------------------------------------------------------------
# Dynamic-site settings
# ------------------------------------------------------------------------------

USE_DYNAMIC_SITE_MIDDLEWARE = True


# ------------------------------------------------------------------------------
# App settings
# ------------------------------------------------------------------------------
SLIDE_THUMBNAIL_SIZE = 250
SLIDE_MAX_HEIGHT = 600
SLIDE_MAX_WIDTH = 1000

SLIDE_TEXT_COLORS = (
    (' ', 'Negro'),
    ('txt_white', 'Blanco'),
)

SLIDE_TEXT_H_POSITION = (
    ('slide_txt_left', 'Izquierda'),
    ('slide_txt_right', 'Derecha'),
)

SLIDE_TEXT_V_POSITION = (
    ('slide_txt_top', 'Arriba'),
    ('slide_txt_bottom', 'Abajo'),
)

# Used by LocaleSelectorMiddleware:
DOMAIN_LANGUAGES = {
    'guineaec.no-ip.org:8000': {
        'name': _('Guinea Ecuatorial'),
        'code': 'es-GQ',
    },
    'cerebellumsrv.no-ip.org:8000': {
        'name': _('Panamá'),
        'code': 'es-PA',
    },
    '127.0.0.1:8000': {
        'name': _('Puerto Rico'),
        'code': 'es-PR',
    },
    'default': {
        'name': _('España'),
        'code': 'es-ES',
    }
}

