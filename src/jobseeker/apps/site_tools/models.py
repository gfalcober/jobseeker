# coding: utf-8

from django.db import models
try:
    from django.utils.timezone import now
except ImportError:
    from datetime import datetime
    now = datetime.now

from jobseeker.apps.site_tools.middlewares import ThreadLocal
from jobseeker import settings


class UpdateTimeBaseModel(models.Model):
    createtime = models.DateTimeField(default=now, editable=False, help_text="Create time")
    lastupdatetime = models.DateTimeField(default=now, editable=False, help_text="Time of the last change.")

    def save(self, *args, **kwargs):
        self.lastupdatetime = now()
        return super(UpdateTimeBaseModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class UpdateUserBaseModel(models.Model):
    # Important: "threadlocals middleware" must be used!
    createby = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False, related_name="%(class)s_createby",
        null=True, blank=True, # <- If the model used outsite a real request (e.g. unittest, db shell)
        help_text="User how create this entry.")
    lastupdateby = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False, related_name="%(class)s_lastupdateby",
        null=True, blank=True, # <- If the model used outsite a real request (e.g. unittest, db shell)
        help_text="User as last edit this entry.")

    def save(self, *args, **kwargs):
        current_user = ThreadLocal.get_current_user()

        if current_user and isinstance(current_user, User):
            if self.pk == None or kwargs.get("force_insert", False): # New model entry
                self.createby = current_user
            self.lastupdateby = current_user

        return super(UpdateUserBaseModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class UpdateInfoBaseModel(UpdateTimeBaseModel, UpdateUserBaseModel):
    class Meta:
        abstract = True

