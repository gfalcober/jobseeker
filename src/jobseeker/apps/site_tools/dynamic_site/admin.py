# coding: utf-8


from django.conf import settings

from django.contrib import admin
from jobseeker.apps.site_tools.dynamic_site.models import SiteAlias


class SiteAliasAdmin(admin.ModelAdmin):
    list_display = (
        "id", "site", "alias", "regex"
    )
    list_display_links = ("id", "alias")
    list_filter = ("site", "regex")
    search_fields = ("alias",)


if getattr(settings, "USE_DYNAMIC_SITE_MIDDLEWARE", False) == True:
    admin.site.register(SiteAlias, SiteAliasAdmin)
