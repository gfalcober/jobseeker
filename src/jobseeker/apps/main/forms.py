# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django import forms

from jobseeker.apps.main.models import Company


class NewCompanyForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super(NewCompanyForm, self).clean()

        # Ensure 'email' and 'name' are unique per-site:
        if Company.on_site.filter(name=cleaned_data['name']).exists():
            raise forms.ValidationError(_('La empresa ' + cleaned_data['name']\
                + ' ya está registrada.'))
        elif Company.on_site.filter(contact_email=cleaned_data['contact_email']).exists():
            raise forms.ValidationError(_('La dirección de correo electrónico '\
                + cleaned_data['contact_email'] + ' ya está registrada.'))

        return cleaned_data

    class Meta:
        model = Company
        fields = ('name', 'contact', 'contact_email', 'comments',)
