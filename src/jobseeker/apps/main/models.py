# -*- coding: utf-8 -*-

from django.db import models
from django.db.models.signals import post_delete
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.dispatch.dispatcher import receiver
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

from jobseeker import settings
from .extra import process_img, get_thumbnail


def get_path(instance, filename):
    object_type = instance.__class__.__name__
    if object_type == 'Slide':
        path = 'img/slides/' + filename
    else:
        path = None
    
    return path


class Base(models.Model):
    objects = models.Manager()
    on_site = CurrentSiteManager()

    class Meta:
        abstract = True


class Country(Base):
    country = models.CharField(max_length=100, primary_key=True, verbose_name=_('País'))
    site = models.ForeignKey(Site, verbose_name=_('Web'))

    def __str__(self):
        return self.country

    class Meta:
        db_table = 'Countries'
        verbose_name = 'País'
        verbose_name_plural = 'países'


class Company(Base):
    name = models.CharField(max_length=150, db_index=True, verbose_name=_('Nombre'))
    name_id = models.CharField(max_length=150, db_index=True, editable=False)
    contact = models.CharField(max_length=150, db_index=True, verbose_name=_('Persona de contacto'))
    contact_email = models.EmailField(max_length=254, verbose_name=_('Email de contacto'))
    comments = models.TextField(verbose_name=_('Comentarios'))
    site = models.ForeignKey(Site, verbose_name=_('Web'))

    def save(self):
        self.name_id = slugify(self.name)
        super(Company, self).save()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Companies'
        verbose_name = 'Empresa'


class Position(Base):
    CURRENCIES = (
        ('app-pepone.rhcloud.com', (
            ('USD', 'USD'),
            ('EUR', 'EUR'),
        )),
        ('guineaec.no-ip.org', (
            ('FCFA', 'FCFA'),
            ('USD', 'USD'),
            ('EUR', 'EUR'),
        )),
        ('vivaelperu.no-ip.org', (
            ('PEN', 'PEN'),
            ('USD', 'USD'),
            ('EUR', 'EUR'),
        )),
    )

    CONTRACT_TYPES = (
        ('Autónomo', _('Autónomo')),
        ('Cuenta ajena', _('Cuenta ajena')),
        ('Otros', _('Otros')),
    )
    
    EXPERIENCE = (
        (0, '< 1'),
        (1, '1 - 3'),
        (3, '3 - 5'),
        (5, '5 - 10'),
        (10, '> 10'),
    )

    company = models.ForeignKey(Company, verbose_name=_('Empresa'))
    state_province = models.CharField(max_length=100, blank=True, null=True, db_index=True, verbose_name=_('Estado/Provincia'))
    city = models.CharField(max_length=100, blank=True, null=True, verbose_name=_('Ciudad'))
    name = models.CharField(max_length=150, db_index=True, verbose_name=_('Nombre del puesto'))
    name_id = models.CharField(max_length=150, db_index=True, editable=False, verbose_name=_('ID para URLs'))
    date = models.DateField(db_index=True, editable=False, auto_now_add=True, verbose_name=_('Fecha de publicación'))
    description = models.TextField(verbose_name=_('Descripción'))
    experience = models.PositiveIntegerField(db_index=True, choices=EXPERIENCE, verbose_name=_('Experiencia (años)'))
    minimum_training = models.CharField(max_length=200, blank=True, null=True, verbose_name=_('Formación mínima'))
    salary = models.DecimalField(max_digits=8, decimal_places=0, blank=True, null=True, db_index=True, verbose_name=_('Salario (bruto anual)'))
    currency = models.CharField(max_length=50, blank=True, null=True, choices=CURRENCIES, verbose_name=_('Moneda'))
    contract = models.CharField(max_length=100, blank=True, null=True, choices=CONTRACT_TYPES, db_index=True, verbose_name=_('Tipo de contrato'))
    keywords = models.CharField(max_length=300, blank=True, null=True, db_index=True, verbose_name=_('Conocimientos'))
    url = models.URLField(verbose_name=_('URL de la oferta'))
    site = models.ForeignKey(Site, editable=False, verbose_name=_('Sitio web'))

    def save(self):
        self.name_id = slugify(self.name)
        self.keywords = slugify(self.keywords)
        self.site = Site.objects.get(id=self.company.site.id)
        
        super(Position, self).save()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Positions'
        verbose_name = 'Oferta'


class Slide(Base):
    image = models.ImageField(upload_to=get_path, max_length=200, verbose_name=_('Imagen'))
    thumbnail = models.ImageField(upload_to=get_path, max_length=200, editable=False)
    text = models.CharField(max_length=500, verbose_name=_('Texto del slide'))
    text_color = models.CharField(max_length=20, choices=settings.SLIDE_TEXT_COLORS, default='Negro', verbose_name=_('Color del texto'))
    text_h_position = models.CharField(max_length=20, choices=settings.SLIDE_TEXT_H_POSITION, default='Izquierda', verbose_name=_('Alineación del texto'))
    text_v_position = models.CharField(max_length=20, choices=settings.SLIDE_TEXT_V_POSITION, default='Arriba', verbose_name=_('Posición del texto'))
    alt_text = models.CharField(max_length=400, verbose_name=_('Texto alternativo (campo "alt")'))
    active = models.BooleanField(db_index=True, default=True, verbose_name=_('Activo'))
    sites = models.ManyToManyField(Site, verbose_name=_('Sitio web'))

    def save(self, **kwargs):
        if self.pk is not None:
            # Object is not new, check if image is being updated:
            previous_file = Slide.objects.get(pk=self.pk)
            storage, path = self.image.storage, self.image.path
            prev_storage = previous_file.image.storage
            if not storage.exists(path) or \
            storage.size(path) != previous_file.image.storage.size(previous_file.image.path):
                # Seems like modifying image, so remove previous one and update:
                # Send post_delete signal to remove previous files:
                post_delete.send(sender=Slide, instance=previous_file)
                self.image = process_img(self)
                self.thumbnail = get_thumbnail(self)
        else:
            # New object
            self.image = process_img(self)
            self.thumbnail = get_thumbnail(self)
    
        super(Slide, self).save()

    def __str__(self):
        return self.image.name

    class Meta:
        db_table = 'Slides'
        verbose_name = 'Slide'


# ------------------------------------------------------------------------------
# Signal handlers
# ------------------------------------------------------------------------------

@receiver(post_delete, sender=Slide)
def slide_post_delete_handler(sender, **kwargs):
    # Remove slides files after the corresponding db entries are deleted or 
    #  prior to updating 'image' field:
    storage, path = kwargs['instance'].image.storage, kwargs['instance'].image.path
    storage.delete(path)
    storage, path = kwargs['instance'].thumbnail.storage, kwargs['instance'].thumbnail.path
    storage.delete(path)
