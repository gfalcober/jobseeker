# -*- coding: utf-8 -*-

from django.views.generic.base import TemplateView, View
from django.shortcuts import render
from django.core.urlresolvers import resolve
from django.core.mail import send_mail

from jobseeker.apps.user_engine.models import User
from jobseeker.apps.main.models import *
from jobseeker.apps.main.forms import *
from jobseeker.apps.main.extra import new_company_email
from .signals import first_login
from jobseeker import settings


# Mixins -----------------------------------------------------------------------
class CurrentSite(object):
    # Adds current site to context, since it's used in most views:
    def get_context_data(self, **kwargs):
        kwargs['site'] = Site.objects.get_current()
        return kwargs


class NewUser(CurrentSite):
    # Adds a welcome message to context and sends signal if the user logs in for the first time:
    def get_context_data(self, **kwargs):
        context = super(NewUser, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated() and not self.request.user.ever_logged_in:
            # Send signal to notify user's first login:
            first_login.send(sender=User, instance=self.request.user)
            context['welcome_message'] = True
        else:
            context['welcome_message'] = False

        return context


# Views ------------------------------------------------------------------------

class Index(NewUser, TemplateView):
    template_name = 'main/index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['slides'] = Slide.on_site.filter(active=True)
        context['positions'] = Position.on_site.order_by('-date')[:6]
        context['country'] = Country.on_site.get()
        return context


class Positions(NewUser, View):
    template_name = 'main/positions.html'
    
    def post(self, *args, **kwargs):
        positions = Position.on_site.filter(name_id__icontains=slugify(self.request.POST['search'])[1:-2])
        
        context = super(Positions, self).get_context_data(**kwargs)
        context['positions'] = positions
        context['detail'] = False
        
        return render(self.request, self.template_name, context)

    def get(self, *args, **kwargs):
        context = super(Positions, self).get_context_data(**kwargs)
        if 'position' in kwargs:
            # Get requested position:
            context['position'] = Position.on_site.get(id=kwargs['id'])
            context['detail'] = True
        else:
            # Get all positions for site:
            context['positions'] = Position.on_site.order_by('-date')
            context['detail'] = False
            
        return render(self.request, self.template_name, context)


class Companies(NewUser, View):
    template_name = 'main/companies.html'

    def post(self, *args, **kwargs):
        context = super(Companies, self).get_context_data(**kwargs)
        context['post'] = True

        form = NewCompanyForm(self.request.POST)
        if form.is_valid():
            new_company = Company(
                name=form.cleaned_data['name'],
                contact=form.cleaned_data['contact'],
                contact_email=form.cleaned_data['contact_email'],
                comments=form.cleaned_data['comments'],
                site=context['site'],
            )
            new_company.save()
            new_company_email(new_company)
            context['company'] = form.cleaned_data['name']
        else: 
            context['validation_error'] = form.errors['__all__']

        return render(self.request, self.template_name, context)

    def get(self, *args, **kwargs):
        context = super(Companies, self).get_context_data(**kwargs)
        context['post'] = False

        if 'company' in kwargs:
            context['positions'] = Position.on_site.filter(company__name_id=kwargs['company'])
            context['company'] = Company.on_site.get(name_id=kwargs['company'])

        return render(self.request, self.template_name, context)


class Candidates(NewUser, TemplateView):
    template_name = 'main/candidates.html'

    def get_context_data(self, **kwargs):
        context = super(Candidates, self).get_context_data(**kwargs)
        context['country'] = Country.on_site.get()
        
        return context


# Views for static pages -------------------------------------------------------

class GenericView(NewUser, TemplateView):
    def get_template_names(self):
        return  [
                   'main/flatpages/'
                   + resolve(self.request.path_info).url_name
                   + '.html'
                ]

    def get_context_data(self, **kwargs):
        context = super(GenericView, self).get_context_data(**kwargs)
        return context
