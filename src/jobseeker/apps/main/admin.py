from django import forms
from django.contrib import admin

from jobseeker.apps.main.models import *


class PositionAdmin(admin.ModelAdmin):
    def country(self):
        return Country.objects.get(site=self.site)

    list_display = ('name', 'company', country, 'city', 'experience', 'salary', 'currency',)
    list_filter = ('site', 'company',)
    search_fields = ('name', 'company',)


class CountryAdmin(admin.ModelAdmin):
    list_display = ('country', 'site',)


class CompanyAdmin(admin.ModelAdmin):
    def country(self):
        return Country.objects.get(site=self.site)

    #readonly_fields = ('name',)
    list_display = ('name', 'site', country, 'contact', 'contact_email',)
    list_filter = ('site',)
    search_fields = ('name',)


class SlideAdmin(admin.ModelAdmin):
    list_display = ('image', 'text', 'active',)
    list_filter = ('sites',)
    search_fields = ('text',)


class SlideChangeForm(forms.ModelForm):
    def save(self, commit=True):
        #a = self.changed_data
        #request.is_bound()
        super(SlideChangeForm, self).save(commit=True, update_fields=self.changed_data)
    

    class Meta:
        model = Slide
        exclude = ('thumbnail',)


admin.site.register(Position, PositionAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Slide, SlideAdmin)
