# -*- encoding: utf-8 -*-

import os

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings

from jobseeker.apps.main.views import *

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^$', Index.as_view(), name='index'),
    url(r'^positions$', Positions.as_view(), name='positions'),
    url(r'^positions/(?P<position>[\w\-]+)-(?P<id>[\d]+)$', Positions.as_view(), name='position'),
    url(r'^companies$', Companies.as_view(), name='companies'),
    url(r'^companies/(?P<company>[\w\-]+)$', Companies.as_view(), name='company_positions'),
    url(r'^candidates$', Candidates.as_view(), name='candidates'),
    # Flatpages:
    url(r'^legal$', GenericView.as_view(), name='legal_notice'),
    url(r'^privacy$', GenericView.as_view(), name='privacy'),
    url(r'^about$', GenericView.as_view(), name='about'),
) 

if 'OPENSHIFT_DATA_DIR' not in os.environ:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) # Not for use in production!
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # Not for use in production!

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^rosetta/', include('rosetta.urls')),
    )
