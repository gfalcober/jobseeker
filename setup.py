from setuptools import setup

setup(name='Jobseeker',
      version='1.0',
      description='Jobseeker App',
      author='Gabriel Faet',
      author_email='gfalcober@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
#      install_requires=requirements.txt,
)
